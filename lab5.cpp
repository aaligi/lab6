// lab5.c //
#include <iostream>

#include <iomanip>

#include <algorithm>

#include <sstream>

#include <string>

#include <time.h>

#include "stdlib.h"

#include "string.h"

using namespace std;

enum Suit { SPADES = 0, HEARTS = 1, DIAMONDS = 2, CLUBS = 3 };

typedef struct Card {

    Suit suit;

    int value;

} Card;

string get_suit_code(Card& c);

string get_card_name(Card& c);

bool suit_order(const Card& lhs, const Card& rhs);

int myrandom(int i) { return std::rand() % i; }

int main(int argc, char const* argv[])

{
    srand(unsigned(time(0)));

    // card array //
    Card deck[52];

    //make deck
    int CounterIndex = 51;
    int SuitNum = 4;
    int FacesNum = 13;

    for (int i = 0; i < SuitNum; i++) { // suit
        for (int j = 0; j < FacesNum; j++) { //faces
            deck[CounterIndex].suit = static_cast<Suit>(i);
            deck[CounterIndex].value = j + 1;
            CounterIndex--;
        }
    }
    // random shuffle //
    random_shuffle(&deck[0], &deck[52], myrandom);

    // random 5 hand //
    Card hand[5];
    for (int i = 0; i < 5; i++) {
        hand[i] = deck[myrandom(52)];
    }
    //sort suit //
    sort(hand, hand + 5, suit_order);

    //print hand //
    for (int i = 0; i < 5; i++) {
        cout << get_card_name(hand[i]) << endl;
    }
    return 0;
}
bool suit_order(const Card& lhs, const Card& rhs)
{
    return lhs.suit < rhs.suit;
}

//return suit code
string get_suit_code(Card& c)
{
    switch (c.suit) {
    case SPADES:
        return "SPADES \u2660";
    case HEARTS:
        return "HEARTS \u2661";
    case DIAMONDS:
        return "DIAMONDS \u2662";
    case CLUBS:
        return "CLUBS \u2663";
    default:
        return "";
    }
}
string get_card_name(Card& c)
{
    stringstream ss;
    //switch card value
    switch (c.value) {
    case 1:
        ss << "Ace";
        break;
        
    // 2-10. Number. 
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
    case 9:
    case 10:
        ss << c.value;
        break;
    case 11: //11-Jack
        ss << "Jack";
        break;
    case 12: //12-Queen
      ss << "Queen";
        break;
    case 13: //13-King
        ss << "King";
        break;
    default:
        ss << "Unknown"; 
    }
    ss << " of " << get_suit_code(c);
    return ss.str();
}